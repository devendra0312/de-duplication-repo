package com.rxcorp.gdcp.duplication

import java.io.File
import java.nio.file.{Files, Paths, StandardCopyOption}

import org.apache.spark.sql.types.{IntegerType, StructField, StructType}
import org.apache.spark.sql.{DataFrame, SparkSession}


case class FileProcessing(sparkSession:SparkSession) {

  /// Function to delete the given number of rows from it ///
  def deleteRows(filename: String, start: Int, num: Int): Unit = {

    import java.nio.file.{Files, Paths}
    val lines = scala.io.Source.fromFile(filename).getLines
    val keep = start.toInt - 1
    val top = lines.take(keep).toList
    val drop = lines.take(num.toInt).toList
    Files.write(Paths.get(filename), scala.collection.JavaConversions.asJavaIterable(top ++ lines))
    //println(lines, keep, top, drop)
    if (top.size < keep || drop.size < num.toInt)
      println(s"Too few lines: removed only ${drop.size} of $num lines starting at $start")

    //   case _ =>
    //     println("Usage: RemoveLinesFromAFile <filename> <startLine> <numLines>")
  }

  /// function ends here///


  /// Directory  validation ///
  def DirectoryValidation(dir: String): String = {
    var status: String = ""
    try {
      val dirName = new File(dir)
      if (dirName.exists())
        status = "Directory does Exist"
      else {
        status = "Directory dose not Exist"
        sys.exit(1)
      }
      status
    }
    catch {
      case e: Exception => {
        println("the exception error is" + e)
        //status = " the exception error occurred :" + e
        //status
        sys.exit(1)
      }
    }
  }

  /// Retrieve number of file in the input in the Given Directory///
  def getCount(inputDir: String): Int = {
    val fileObj = new File(inputDir)
    var count: Int = 0
    try {
      if (fileObj.exists()) {
        for (file <- new File(inputDir).listFiles()) {
          /*var sourceFileNames: String = inputDir + "\\" + file.getName
              var archiveFileName: String = archiveDir + "\\" + file.getName
              val task = Files.move(Paths.get(sourceFileNames), Paths.get(archiveFileName), StandardCopyOption.REPLACE_EXISTING)
              if (task != null)
                status = true
              else
                status = false */
          count = count + 1
        }
        //println(" the number of files are " + count)
      }
      count
    }
    catch {
      case e: Exception =>
        println("the exception error is " + e)
        sys.exit(1)
    }
  }


  /// checking is file exist and copy to destination dir ///
  def filesCopyFun(sourceDir: String, destinationDir: String): Boolean = {
    var status: Boolean = false
    val fileObj = new File(sourceDir)
    var count: Int = 0
    try {
      if (fileObj.exists()) {
        for (file <- new File(sourceDir).listFiles()) {
          var sourceFileNames: String = sourceDir + "\\" + file.getName
          var archiveFileName: String = destinationDir + "\\" + file.getName
          val task = Files.copy(Paths.get(sourceFileNames), Paths.get(archiveFileName), StandardCopyOption.REPLACE_EXISTING)
          if (task != null)
            status = true
          else
            status = false
          count = count + 1
        }
        //println(" the number of files are " + count)
      }
      status
    }
    catch {
      case e: Exception =>
        println("the exception error is " + e)
        sys.exit(1)
    }
  }

  /// Move given File to Destination Directory///

  def moveFileToDir(fileName: String, dir: String): Unit = {

    val path = Files.move(
      Paths.get(fileName),
      Paths.get(dir),
      StandardCopyOption.REPLACE_EXISTING
    )

    if (path != null) {
      println(s"The $fileName file was moved successfully to $dir")
    } else {
      println(s"Exception Error occurred in moving the file $fileName")
    }

  }

  /// Delete the given File///
  def deleteFile(file: String): Unit = {
    val fileTemp = new File(file)
    if (fileTemp.exists) {
      fileTemp.delete()
    }
  }

  ///  Change the schema of the Dataframe///

  def editSChema(df: DataFrame, cn: String, nullable:Boolean) : DataFrame = {

    val schema = df.schema
    val newSchema = StructType(schema.map {
      case StructField( c, t, _, m) if c.equals(cn) => StructField( c, t, nullable = nullable, m)
      case y: StructField => y
    })
    df.sqlContext.createDataFrame( df.rdd, newSchema)
  }

  /// Add column to DataFrame ///
  import org.apache.spark.sql.functions.lit
  def addColToDf(df: DataFrame, list: List[String]): DataFrame = {
    var newDf = df.withColumn("DEL_IND", lit(list(0)).cast(IntegerType))
      .withColumn("SUPPLIER_CD", lit(list(1)))
      .withColumn("EXTRNL_PHARMACY_ID", lit(list(2)))
      .withColumn("SUPPLIER_PHARMACY_CD", lit(list(3)))
      .withColumn("PHARMACY_NM", lit(list(4)))
      .withColumn("PHARMACY_ADDR", lit(list(5)))
      .withColumn("PAT_ID", lit(list(6)))
      .withColumn("PAT_NATL_REF_NBR", lit(list(7)))
      .withColumn("PAT_SEX", lit(list(8)))
      .withColumn("PAT_YOB", lit(list(9)).cast(IntegerType))
      .withColumn("RXER_ID", lit(list(10)))
      .withColumn("SRC_FILE_ID", lit(list(11)).cast(IntegerType))
      newDf=editSChema(newDf,"DEL_IND",true)
      newDf=editSChema(newDf,"SUPPLIER_CD",true)
      newDf=editSChema(newDf,"EXTRNL_PHARMACY_ID",true)
      newDf=editSChema(newDf,"SUPPLIER_PHARMACY_CD",true)
      newDf=editSChema(newDf,"PHARMACY_NM",true)
      newDf=editSChema(newDf,"PHARMACY_ADDR",true)
      newDf=editSChema(newDf,"PAT_ID",true)
      newDf=editSChema(newDf,"PAT_NATL_REF_NBR",true)
      newDf=editSChema(newDf,"PAT_SEX",true)
      newDf=editSChema(newDf,"PAT_YOB",true)
      newDf=editSChema(newDf,"RXER_ID",true)
      newDf
  }


  ///here is the code to read the input csv file and return as dataframe///
  def readCsvFile(inputFile: String): DataFrame = {
    try {
      val df = sparkSession.read.
        format("csv").
        option("header", "true").
        schema("customSchema").
        option("delimiter", ",").
        csv(inputFile).
        where("COL1===D")
      //load(inputFile).where("col1"==="D")
      df
    }
    catch {
      case e: Exception =>
        println(" the exception occurred during CVS file reading is " + e)
        sys.exit(1)
    }
  }

  ///here is the code to valaidate the csv file */
  def validateDfFormat(df: DataFrame): Boolean = {
    var format: Boolean = false
    try {
      if (df.columns.contains("H") || df.columns.contains("h")) {
        df.createTempView("table")
        val tempDf = sparkSession.sql("select H from table where H='D' or H='d'")
        if (tempDf.count() > 0)
          format = true
      }
      format
    }
    catch {
      case e: Exception =>
        println(" Error occurred in validate DfFormat function is " + e)
        sys.exit(1)
    }
  }




  //fileRenameAndMove(fn, outputDirPrc + s"\\$fn",outputDirPrc)
/// Function to rename and move output geneeated file

}

