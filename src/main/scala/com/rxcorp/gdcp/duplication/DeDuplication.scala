package com.rxcorp.gdcp.duplication
import java.io.File

import com.rxcorp.gdcp.duplication.Configuration._
import org.apache.log4j.{BasicConfigurator, Level, LogManager, Logger}
import org.apache.spark.sql.functions.lit
import org.apache.spark.sql.types.IntegerType
import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}

import scala.collection.mutable.Map

object DeDuplication {

 def main(args: Array[String]): Unit = {

   BasicConfigurator.configure()

   Logger.getLogger("org").setLevel(Level.OFF)
   Logger.getLogger("akka").setLevel(Level.OFF)



   def releasDF(dataF:DataFrame):DataFrame={
     dataF.unpersist()
   }
   /// Creating Spark Object///
   val sparkObj = SparkSession.builder
      .master("local")
      .appName("DeDuplicationApp")
      .getOrCreate()

   LogManager.getRootLogger.setLevel(Level.OFF)
   //log4j.logger.org.eclipse.jetty=WARN

    /// Creating reference for each Class///
    val logTrans=LogTransmission(sparkObj)
    val readWriteDb=ReadWriteDB(sparkObj)
    val fileProcessing=FileProcessing(sparkObj)
    val dataValidation=DataValidation(sparkObj)

    /// Step : checking if the directory exist or not AND Creating Hash Map Message for Kafka server///
    val hashMap: Map[String, String] = Map(("Event Time", curTime),  ("Message", " Validating Directory for Input Files"))
    logTrans.hashMapFunc(hashMap)
    val dirStatus: String = fileProcessing.DirectoryValidation(inputDir)
    hashMap("Message") = dirStatus
    logTrans.hashMapFunc(hashMap)

      /// Step :  Code to check if the Files exists or not and fetching their count if exists  ///
      hashMap("Message") = "Checking if the File Exist in the Input Directory"
      logTrans.hashMapFunc(hashMap)
      val count: Int = fileProcessing.getCount(inputDir)
      if (count == 0) {
      hashMap("Message") = "No Files Found in the Input Directory"
      logTrans.hashMapFunc(hashMap)
      sys.exit()
      }
      else {
      hashMap("Message") = s"$count File(s) Found in the Input Directory"
      logTrans.hashMapFunc(hashMap)
      }

      /// Step : Copy the Files from the input directory to the working directory ///
       hashMap("Message") = "Initiating the File Transfer From Input Folder to Working Directory"
      logTrans.hashMapFunc(hashMap)
      val fileCopiedStatus: Boolean = fileProcessing.filesCopyFun(inputDir, workingDir)
      if (fileCopiedStatus) {
      //println(" Message: The Files were moved successfully")
      hashMap("Message") = "The Files were Moved Successfully to Working Directory"
      logTrans.hashMapFunc(hashMap)
      }
      else {
      //println("Message: There was some error in moving the files")
      hashMap("Message") = "Some Error Occurred in Moving the File from the working Directory"
      logTrans.hashMapFunc(hashMap)
       }


      //************************************************************************************************************//

      /// File Processing Start From here**********///
      hashMap("Message") = "Initiating the File Processing"
      logTrans.hashMapFunc(hashMap)

    ///Step : Reteriving all the file name  working directory and processing on it //
      var file_name: String = ""  // Creating Empty File
      val fileObj = new File(workingDir)
      if (fileObj.exists()) {
        for (file <- new File(workingDir).listFiles()) {
          file_name = file.getName
          println("The File Under Process is : "+ file_name)

          /// Step : TO read read first row only ///
          val bufferedSource = scala.io.Source.fromFile(workingDir+"\\"+file_name)
          val col2 = bufferedSource.getLines().toList.take(1).mkString.replace("\",\"", "~")
          val col3 = col2.replace("\",", "~").replace(",\"", "~").split("~").toList
          val supID = col3(2)
          val supName = col3(3)
          val supAddress = col3(4)
          hashMap("Message") = "The Supplier Details Obtained"
          hashMap += ("Supplier ID" -> s"$supID", "Supplier Name" -> s"$supName", "Supplier Address" -> s"$supAddress")
          logTrans.hashMapFunc(hashMap)


          val fileNameToList: List[String] = file_name.split("_").toList
          val SUPPLIER_CD_NAME = fileNameToList(1)
          val SUPPLIER_CD = readWriteDb.getSupplierCode(SUPPLIER_CD_NAME)
          val DEL_IND = 1
          val EXTRNL_PHARMACY_ID = fileNameToList(2)
          val SUPPLIER_PHARMACY_CD = fileNameToList(1) + "_" + fileNameToList(2)
          val PHARMACY_NM = supName
          val PHARMACY_ADDR = supAddress
          val PAT_ID = null
          val PAT_NATL_REF_NBR = null
          val PAT_SEX = " "
          val PAT_YOB = null
          val RXER_ID = null

          hashMap += ("Process ID"->s"$processID")
          hashMap("Message") = s"Processing File name : $file_name"
          logTrans.hashMapFunc(hashMap)

          /// Step : validate the CSV file is in correct format ///
          hashMap("Message") = "Initiating File Format Validation"
          logTrans.hashMapFunc(hashMap)

          val inputDf = sparkObj.read.option("Header", "true").option("delimiter", ",").csv(workingDir+"\\"+file_name)
          val csvFormat: Boolean = fileProcessing.validateDfFormat(inputDf)
          if (csvFormat) {
            //println(" The format of the csv file is in correct")
            hashMap("Message") = "File Format validation Passed"
            logTrans.hashMapFunc(hashMap)
          }
          else {
            //println(" the format of the csv file is not in the correct format")
            hashMap("Message") = "File Format validation Failed"
            logTrans.hashMapFunc(hashMap)
            sys.exit()
          }


          /// Updating PS_FIlE Oracle Table and fetching SRC_FILE_ID///
          val SRC_FILE_ID=readWriteDb.getSrcFileId()
          //println(" the SRC ID is "+ SRC_FILE_ID)

          /// Inserting the record to PS_FILE///
          readWriteDb.insertToPsFileTable(SRC_FILE_ID,file_name,SUPPLIER_CD,file_name,"In-Progress")

          /// Step : Remove the first two row of the text file///
          fileProcessing.deleteRows(workingDir+"\\"+file_name, 1, 2)

          /// Loading the Working file to DataFrame ///
          var df1x = sparkObj.read.option("header", "true").schema(customSchema).option("delimiter", ",").csv(workingDir+"\\"+file_name)
          var df1=df1x.filter((df1x("REC_TYP").rlike("D")))

          ///Creating List ///
          val dfColList = List(s"$DEL_IND",s"$SUPPLIER_CD",s"$EXTRNL_PHARMACY_ID",s"$SUPPLIER_PHARMACY_CD",s"$PHARMACY_NM",s"$PHARMACY_ADDR",s"$PAT_ID",s"$PAT_NATL_REF_NBR",s"$PAT_SEX",s"$PAT_YOB",s"$RXER_ID",s"$SRC_FILE_ID")

          ///Adding Columns to DataFrame///
          df1 = fileProcessing.addColToDf(df1, dfColList)
          hashMap("Message") = "All the Columns added to DataFrame"
          logTrans.hashMapFunc(hashMap)


          /// Retrieving Details Column and Key Columns and Hash code///
          val rawDf = dataValidation.getColHashCode(df1).createTempView("Table1")

          ///Creating DF  as per Client table///
          val df:DataFrame=sparkObj.sql("select SUPPLIER_CD,EXTRNL_PHARMACY_ID, REC_TYP,RX_TYP,RX_DSPNSD_DT, RX_DSPNSD_TM ,RX_REPEAT_STATUS ,RXER_ID , PAT_ID,PAT_NATL_REF_NBR, PAT_SEX,PAT_YOB EXMT_STATUS,RX_ID , RX_ITEM_SEQ, SUPPLIERS_DSPNSD_DRUG_CD , DSPNSD_DRUG_IPU_CD , DSPNSD_DRUG_DESC , GENERIC_USE_MARKER, DSPNSD_QTY, DSPNSD_UNIT_OF_QTY , DSPNSD_DRUG_PACK_SIZE , SUPPLIERS_PSCR_DRUG_CD , PSCR_DRUG_IPU_CD, PSCR_DRUG_DESC, PSCR_QTY,VERBOSE_DOSAGE, COST_OF_DSPNSD_QTY, NRSG_HM_IND, TRANS_GUID, KEY_CLMNS_HASH, DETL_CLMNS_HASH, SUPPLIER_PHARMACY_CD, PHARMACY_NM, PHARMACY_ADDR, DEL_IND from Table1")
          //val df:DataFrame=sparkObj.sql("select SUPPLIER_CD,EXTRNL_PHARMACY_ID, REC_TYP,RX_TYP, RX_DSPNSD_TM ,RX_REPEAT_STATUS ,RXER_ID , PAT_ID,PAT_NATL_REF_NBR, PAT_SEX,PAT_YOB EXMT_STATUS,RX_ID , RX_ITEM_SEQ, SUPPLIERS_DSPNSD_DRUG_CD , DSPNSD_DRUG_IPU_CD , DSPNSD_DRUG_DESC , GENERIC_USE_MARKER, DSPNSD_QTY, DSPNSD_UNIT_OF_QTY , DSPNSD_DRUG_PACK_SIZE , SUPPLIERS_PSCR_DRUG_CD , PSCR_DRUG_IPU_CD, PSCR_DRUG_DESC, PSCR_QTY,VERBOSE_DOSAGE, COST_OF_DSPNSD_QTY, NRSG_HM_IND, TRANS_GUID,SUPPLIER_PHARMACY_CD, PHARMACY_NM, PHARMACY_ADDR, DEL_IND from Table1")

          println(" the Raw DF is : ")
          df.show()

          hashMap("Message") = "All the required column and data loaded to dataFrame"
          logTrans.hashMapFunc(hashMap)

          /// Filtering unique records for client table///
          val clientTableDF=readWriteDb.getUniqueRecord(df, oracleDbClientTable,"DETL_CLMNS_HASH")
          println("The final client Records to insert in Database is  : ")
          clientTableDF.show()
//            if(clientTableDF.count()==0 && df.count()>0)
//              {
//                fileProcessing.moveFileToDir(inputDir+s"\\$file_name",duplicateDir+s"\\$file_name _duplicate")
//                println("Duplicate File was copied to the duplicate directory")
//              }


          hashMap("Message") = "All the required column and unique records sorted out"
          logTrans.hashMapFunc(hashMap)

          /// Writing the DataFrame to CSV File is ///
          //readWriteDb.writeToCSV(clientTableDF, "clientTableDF")

          /// Step: Writing the DataFrame to Client Database ///
            readWriteDb.writeToDbTable(clientTableDF,oracleDbClientTable)
            hashMap("Message") = s"Records updated to $oracleDbClientTable"
            logTrans.hashMapFunc(hashMap)


            /*****************Working for the History table and Client table******/

                /// Creating Dataframe for the history Table///
                var historyTableDF=sparkObj.sql("select TRANS_GUID,DETL_CLMNS_HASH,KEY_CLMNS_HASH,RX_DSPNSD_DT,SUPPLIER_PHARMACY_CD,DEL_IND from Table1")

                /// Filtering unique records for History table///
                 historyTableDF=readWriteDb.getUniqueRecord(historyTableDF, oracleDbhistoryTable,"DETL_CLMNS_HASH")
                println(" The Unique Records for history table is ")
                historyTableDF.show()

                /// Updating the History table record in the Databasae///
                readWriteDb.writeToDbTable(historyTableDF,oracleDbhistoryTable)

                /// Creating Dataframe for the Update Table///
                var updateTableDF=sparkObj.sql("select TRANS_GUID,SUPPLIER_PHARMACY_CD,RX_DSPNSD_DT,KEY_CLMNS_HASH,DETL_CLMNS_HASH from Table1")

                /// Fetching Unique record for the Update Table
                updateTableDF=readWriteDb.getUniqueRecord(updateTableDF, oracleDbupdateTable,"KEY_CLMNS_HASH")
                updateTableDF.withColumn("SRC_FILE_ID", lit(SRC_FILE_ID).cast(IntegerType))
                println("the update table")
                updateTableDF.show()

                /// Updating the History table record in the Databasae///
                readWriteDb.writeToDbTable(updateTableDF,oracleDbupdateTable)

                ///Step to Save Dataframe in CSV format to Output Directory//
                //println("writing to csv table")
                readWriteDb.writeToCSV(clientTableDF, file_name)

                hashMap("Message") = s"Records saved in CSV file as to $file_name in Output Directory"
                logTrans.hashMapFunc(hashMap)

                /// Step : Move the Files from the input directory to the Archive directory///
                fileProcessing.moveFileToDir(inputDir+s"\\$file_name",archiveDir+s"\\$file_name")
                hashMap("Message") = "The Files were moved successfully from the Input Directory to Archive Directory"
                logTrans.hashMapFunc(hashMap)

                /// Step to Delete the processed file///
                fileProcessing.deleteFile(workingDir+s"\\$file_name")
                hashMap("Message") = "The Processed Files was Deleted successfully from the Working Directory"
                logTrans.hashMapFunc(hashMap)

                /// Update PS_FILE table for the Processed File///
                readWriteDb.updateToPsFileTable(SRC_FILE_ID,"SUCCESS")

          //releasDF(df)
          //releasDF()
        }

      }
    }
}