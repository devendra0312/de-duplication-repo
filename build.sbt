name := "DeDuplicationProj"

version := "0.1"
scalaVersion := "2.11.11"

fork in run := true
javaOptions in run ++= Seq(
    "-Dlog4j.debug=true",
    "-Dlog4j.configuration=/data/sit/ie/ie_lrx/scripts/log4j.properties")
    //"-Dlog4j.configuration=log4j.properties")
outputStrategy := Some(StdoutOutput)


lazy val config = (project in file("config"))
  .settings(
    organization := "com.rxcorp.gdcp.duplication",
    name := "config",
    version := "1.0.0-SNAPSHOT",
    scalaVersion := "2.11.11")
  .settings(libraryDependencies ++= Seq("org.apache.spark" %% "spark-core" % "2.3.0",
    "com.typesafe" % "config" % "1.3.1",
    "org.apache.spark" %% "spark-sql" % "2.3.0",
    "org.apache.commons" % "commons-io" % "1.3.2",
      "log4j" % "log4j" % "1.2.17",
    "mysql" % "mysql-connector-java" % "8.0.11"))

lazy val messagingsystem = (project in file("messagingsystem"))
  .settings(
    organization := "com.rxcorp.gdcp.duplication",
    name := "messagingsystem",
    version := "1.0.0-SNAPSHOT",
    scalaVersion := "2.11.11")
  .settings(libraryDependencies ++= Seq("org.apache.kafka" % "kafka-clients" % "2.1.0",
    "com.google.code.gson" % "gson" % "2.8.4"
  )).dependsOn(`config`)

lazy val fileprocessing = (project in file("fileprocessing"))
  .settings(
    organization := "com.rxcorp.gdcp.duplication",
    name := "fileprocessing",
    version := "1.0.0-SNAPSHOT",
    scalaVersion := "2.11.11")
  .settings(libraryDependencies ++= Seq("ch.qos.logback" % "logback-classic" % "1.1.7",
    "com.typesafe.scala-logging" %% "scala-logging" % "3.4.0",
    "com.github.pathikrit" %% "better-files" % "3.4.0","com.databricks" % "spark-csv_2.11" % "1.5.0"))
  .dependsOn(`config`,`messagingsystem`)
lazy val datavalidation = (project in file("datavalidation"))
  .settings(
    organization := "com.rxcorp.gdcp.duplication",
    name := "datavalidation",
    version := "1.0.0-SNAPSHOT",
    scalaVersion := "2.11.11")
  .settings(libraryDependencies ++= Seq())
  .dependsOn(`config`,`messagingsystem`, `fileprocessing`)
lazy val readwritedb = (project in file("readwritedb"))
  .settings(
    organization := "com.rxcorp.gdcp.duplication",
    name := "readwritedb",
    version := "1.0.0-SNAPSHOT",
    scalaVersion := "2.11.11")
  .settings(libraryDependencies ++= Seq())
  .dependsOn(`config`,`messagingsystem`, `fileprocessing`,`datavalidation`)
lazy val deduplication = (project in file("."))
  .settings(name := "duplication",
    organization := "com.rxcorp.gdcp.duplication",
    version := "1.0.0-SNAPSHOT",
    scalaVersion := "2.11.11")
  .aggregate(config)
  .settings(libraryDependencies ++= Seq(
  )).dependsOn(`config`, `messagingsystem`,`fileprocessing`, `datavalidation`,`readwritedb`)



// META-INF discarding
mergeStrategy in assembly <<= (mergeStrategy in assembly) { (old) =>
{
    case PathList("META-INF", xs @ _*) => MergeStrategy.discard
    case x => MergeStrategy.first
}
}