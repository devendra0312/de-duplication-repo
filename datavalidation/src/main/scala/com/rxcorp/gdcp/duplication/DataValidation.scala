package com.rxcorp.gdcp.duplication
import org.apache.spark.sql.{DataFrame, SparkSession}
import java.security.MessageDigest
import java.sql.Date

import org.apache.avro.generic.GenericData.StringType
import org.apache.spark.sql.types.{DateType, DoubleType, IntegerType}

case class DataValidation(sparkSession:SparkSession) {

  def getColHashCode(df: DataFrame): DataFrame = {

    def toHex(bytes: Array[Byte]): String = bytes.map("%02x".format(_)).mkString("")

    def md5_tmp = org.apache.spark.sql.functions.udf({ (c1: String, c2: String, c3: String, c4: String, c5: String, c6: String, c7: String, c8: String, c9: String, c10: String) =>

      val a = c1.concat(c2).concat(c3).concat(c4).concat(c5).concat(c6.toString).concat(c7.toString).concat(c8).concat(c9).concat(c10)
      toHex(MessageDigest.getInstance("MD5").digest(a.getBytes("UTF-8")))
    })

    def md5_dtl_col = org.apache.spark.sql.functions.udf({ (c1: String, c2: String, c3: Int, c4:Double, c5:Int) =>
      val a = c1.concat(c2).concat(c3.toString).concat(c4.toString).concat(c5.toString)
      toHex(MessageDigest.getInstance("MD5").digest(a.getBytes("UTF-8")))
    })

    def md5_key_col = org.apache.spark.sql.functions.udf({ (c1: String, c2: String, c3: String, c4:String, c5: String) =>
      val a = c1.concat(c2).concat(c3).concat(c4).concat(c5.toString)
      toHex(MessageDigest.getInstance("MD5").digest(a.getBytes("UTF-8")))
    })

    // creating md5
    df.createOrReplaceTempView("test1")

    def dbms_crypto(s:String) : String = {
      // Create md5 of the string
      val digest = MessageDigest.getInstance("MD5")
      val md5hash = digest.digest(s.getBytes).map(0xFF & _).map { "%02x".format(_) }.foldLeft(""){_ + _}
      return md5hash.map(_.toUpper)
    }

    sparkSession.udf.register("dbms_crypto",dbms_crypto _)


    //var new_df=sparkSession.sql("select * FROM TEST1 ")

    var new_df=sparkSession.sql("select *, dbms_crypto(CONCAT(RX_TYP,RX_REPEAT_STATUS,EXMT_STATUS,SUPPLIERS_DSPNSD_DRUG_CD,DSPNSD_DRUG_IPU_CD,GENERIC_USE_MARKER,DSPNSD_QTY,DSPNSD_UNIT_OF_QTY,DSPNSD_DRUG_PACK_SIZE,SUPPLIERS_PSCR_DRUG_CD,PSCR_DRUG_IPU_CD,PSCR_QTY,COST_OF_DSPNSD_QTY,NRSG_HM_IND)) DETL_CLMNS_HASH, dbms_crypto(CONCAT(SUPPLIER_CD,EXTRNL_PHARMACY_ID,RX_ID,RX_ITEM_SEQ,RX_DSPNSD_DT)) KEY_CLMNS_HASH FROM TEST1 ")

//    new_df.show()
//
//    sys.exit()
//
//    def betterLowerRemoveAllWhitespace(s: String): Option[String] = {
//      val str = Option(s).getOrElse(return None)
//      Some(str.toLowerCase().replaceAll("\\s", ""))
//    }

    //sparkSession.sql("select dbms_crypto(CONCAT(RX_TYP,RX_REPEAT_STATUS,EXMT_STATUS,SUPPLIERS_DSPNSD_DRUG_CD,DSPNSD_DRUG_IPU_CD,GENERIC_USE_MARKER,DSPNSD_QTY,DSPNSD_UNIT_OF_QTY,DSPNSD_DRUG_PACK_SIZE,SUPPLIERS_PSCR_DRUG_CD,PSCR_DRUG_IPU_CD,PSCR_QTY,COST_OF_DSPNSD_QTY,NRSG_HM_IND)) DETL_CLMNS_HASH  from test1").show()

//    sys.exit()
//    var new_df = df.withColumn("DETL_CLMNS_HASH_temp",
//      md5_tmp(df.col("RX_TYP"),
//        df.col("RX_REPEAT_STATUS"),
//        df.col("EXMT_STATUS"),
//        df.col("SUPPLIERS_DSPNSD_DRUG_CD"),
//        df.col("DSPNSD_DRUG_IPU_CD"),
//        df.col("GENERIC_USE_MARKER"),
//        df.col("DSPNSD_QTY"),
//        df.col("DSPNSD_UNIT_OF_QTY"),
//        df.col("DSPNSD_DRUG_PACK_SIZE"),
//        df.col("SUPPLIERS_PSCR_DRUG_CD")
//      ))
//
//    new_df = new_df.withColumn("DETL_CLMNS_HASH",
//      md5_dtl_col(new_df.col("DETL_CLMNS_HASH_temp"),
//        new_df.col("PSCR_DRUG_IPU_CD"),
//        new_df.col("PSCR_QTY").cast(IntegerType),
//        new_df.col("COST_OF_DSPNSD_QTY").cast(DoubleType),
//        new_df.col("NRSG_HM_IND").cast(IntegerType)))
//      .drop("DETL_CLMNS_HASH_temp")
//
//    new_df = new_df.withColumn("KEY_CLMNS_HASH",
//      md5_key_col(new_df.col("SUPPLIER_CD"),
//        new_df.col("EXTRNL_PHARMACY_ID"),
//        new_df.col("RX_ID"),
//        new_df.col("RX_ITEM_SEQ"),
//        new_df.col("RX_DSPNSD_DT")))

        // Here the RX_DSPNSD_DT column provided is String type in the input file hence it is type casted to Date Type//
        new_df= new_df.withColumn("RX_DSPNSD_DT_new", new_df("RX_DSPNSD_DT").cast(DateType)).drop("RX_DSPNSD_DT")
        new_df = new_df.withColumnRenamed("RX_DSPNSD_DT_new", "RX_DSPNSD_DT")

        new_df
  }


}

