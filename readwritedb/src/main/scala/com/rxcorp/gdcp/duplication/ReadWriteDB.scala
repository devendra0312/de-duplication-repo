package com.rxcorp.gdcp.duplication
import java.io.File
import java.nio.file.{Files, Paths, StandardCopyOption}

import org.apache.spark.sql.{DataFrame, SparkSession}
import com.rxcorp.gdcp.duplication.Configuration._
import oracle.jdbc.pool.OracleDataSource
import java.sql.{Connection, Date}
import java.sql.PreparedStatement
import java.util.Calendar

import org.apache.spark.sql.types.{StructField, StructType}
case class ReadWriteDB(sparkSession:SparkSession) {

      /// Oracle ODC Connection ///
      def getConnection():Connection={
      try {
          val ods = new OracleDataSource()
          ods.setUser(oracleDbUserName)
          ods.setPassword(oracleDbPassword)
          ods.setURL(oracleDbUrl)
          val con:Connection = ods.getConnection()
          //println("Connected " + con)
          con
      }
      catch {
          case (e: Exception) =>
          println("Exception Error Occurred during connection with the Oracle Database" + e)
          sys.exit()
      }
      }

        /*
        /// JDBC Connection for Connection///
        def getConnection():Connection= {
        try {
          Class.forName("oracle.jdbc.driver.OracleDriver")
          val conn: Connection = DriverManager.getConnection(oracleDbUrl, oracleDbUserName, oracleDbPassword)
          println(conn)
          conn
        }
        catch {
          case (e: Exception) =>
          println("Exception Error Occurred during connection with the Oracle Database" + e)
          sys.exit()
          }
          }
          */

        /// Writing Dataframe to Oracle Database///
        def writeToDbTable(df: DataFrame, table:String): Unit = {

        try {
          df.write.mode("Append").jdbc(oracleDbUrl, table, connectionProperties)
        }
        catch {
          case e: Exception =>
            println(s"Error in writing to the Database $table Table $e")
            sys.exit()
        }
        }

        /// Function to Save DF to CSV file///
        def writeToCSV(df:DataFrame,fileNm:String):Unit= {
        try {
          df.coalesce(1).write.option("header", "true").mode("overwrite").csv(outputDir + s"\\$fileNm")
          val fileObj = new File(outputDir+s"\\$fileNm")
          if (fileObj.exists()) {
            for (file <- new File(outputDir+s"\\$fileNm").listFiles()) {
            var file_name = file.getName
            if (file_name.contains(".crc")|| file_name.contains("SUCCESS"))
              file.delete()
            else {
            val task = Files.move(Paths.get(s"$outputDir\\$fileNm\\$file_name"), Paths.get(s"$outputDir\\$fileNm.csv"), StandardCopyOption.REPLACE_EXISTING)
            }
            }
            if (fileObj.isDirectory)
            fileObj.delete()
          }
        }
        catch {
          case e: Exception =>
          println(s" the exception error occurred in creating $fileNm " + e)
          sys.exit()
        }
        }

        /// Fetching the Unique records from the Dataframe for the same table in the database///
        def getUniqueRecord(df:DataFrame,tableNM:String,colName:String):DataFrame={
          val dbTableDF = sparkSession.read.jdbc(oracleDbUrl, tableNM, connectionProperties)
          dbTableDF.createOrReplaceTempView("DbTable1")
          df.createOrReplaceTempView("DfTable1")
          var finalUniqueRecordDf = sparkSession.sql(s"select * from DfTable1 where $colName not in (select $colName from DbTable1)")
          finalUniqueRecordDf
        }

        /// Get Supplier Code Form the Database//
        def getSupplierCode(name: String): Int= {
        var id=0
          var con:Connection=null
        try {

          con = getConnection()
          //println(" the connection string is "+con)
          val cs = con.createStatement()
          val rs = cs.executeQuery(s"select ORG_ID from PS_ORG where ORG_SHORT_NM='$name'")
          while (rs.next())
            id = rs.getInt(1)
            con.close()
            //println(" the id obtained is " + id)
            id
        }
        catch {
          case (e: Exception) =>
            con.close()
            println(" the exception Error " + e)
            sys.exit()
        }
        }

        /// Retrieving the Source FIle ID from the Database///
        def getSrcFileId():Int= {
          var con: Connection = null
          var id =0
          try {
            con = getConnection()
            val cs = con.createStatement()
            // sequence name is databas is DPC_DEDUP_SEQUENCER/
            val rs = cs.executeQuery("SELECT DCP_DEDUP_SEQUENCER.NEXTVAL FROM dual")
            while (rs.next())
              id = rs.getInt(1)
              con.close()
              id
          }
          catch {
            case (e: Exception)=>
              println(" the Exception error occured retrieving the SRC_FILE_ID is :"+ e)
              con.close()
              sys.exit()
          }
        }
      /// Insert record to PS_FILE Table///
      def insertToPsFileTable(srdFileId:Int,outputFileName:String,orgID:Int,inputFileName:String,status:String):Unit={
        var con:Connection=null
        try{
          val calendar: Calendar = Calendar.getInstance
          val startDate = new Date(calendar.getTime.getTime)
          val con=getConnection()
          val cs=con.createStatement()
          //val rs=cs.executeUpdate(s"insert into PS_FILE SRC_FILE_ID,FILE_NM,ORG_ID,INPUT_FILE_NM,FILE_STATUS values ($srdFileId,$outputFileName,$orgID,$inputFileName,$status)")
          val query: String = " insert into PS_FILE (SRC_FILE_ID,FILE_NM,FILE_PROC_DT,ORG_ID,INPUT_FILE_NM,FILE_STATUS) values (?, ?, ?, ?, ?, ?)"
          // create the mysql insert preparedstatement
          val preparedStmt: PreparedStatement = con.prepareStatement(query)
          preparedStmt.setInt(1, srdFileId)
          preparedStmt.setString(2, outputFileName)
          preparedStmt.setDate(3, startDate)
          preparedStmt.setInt(4,orgID)
          preparedStmt.setString(5, inputFileName)
          preparedStmt.setString(6, status)
          // execute the preparedstatement
          preparedStmt.execute
          con.close()
        }
        catch {
          case (e: Exception) =>
            println("Exception error in writing to table" + e)
            con.close()
            sys.exit()
        }
      }

    /// Update PS FILE table///
    /// Insert record to PS_FILE Table///
    def updateToPsFileTable(srdFileId:Int,status:String):Unit={
      var con:Connection=null
      try{
        val calendar: Calendar = Calendar.getInstance
        val startDate = new Date(calendar.getTime.getTime)
        //println("Query Execution Date: "+startDate)
        val con=getConnection()
        val cs=con.createStatement()
        val query=s"update PS_FILE set FILE_STATUS='$status',FILE_PROC_DT=$startDate where SRC_FILE_ID=$srdFileId"
        //println(query)
        val preparedStmt:PreparedStatement=con.prepareStatement(query)
        con.close()
      }
      catch {
        case (e: Exception) =>
          println("Exception error in writing to table" + e)
          con.close()
          sys.exit()
      }
    }



}
