package com.rxcorp.gdcp.duplication
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.{Calendar, Properties, UUID}
import com.typesafe.config.ConfigFactory
import org.apache.spark.sql.types._
object Configuration {

  ///Process ID generation///
  val processID = UUID.randomUUID().toString

  /// Current Time ///
  val curTime:String=LocalDateTime.now.format(DateTimeFormatter.ofPattern("dd-MM-YYYY hh:mm:ss.SSS a"))


  val oracleDbHost = ConfigFactory.load().getString("parameter.oracle.dbHostName")
  val oracleDbPort = ConfigFactory.load().getString("parameter.oracle.dbPort")
  val oracleDbDatabase = ConfigFactory.load().getString("parameter.oracle.dbDatabase")
  val oracleDbUserName = ConfigFactory.load().getString("parameter.oracle.dbUserName")
  val oracleDbPassword = ConfigFactory.load().getString("parameter.oracle.dbPassword")
  val oracleDbClientTable = ConfigFactory.load().getString("parameter.oracle.clientTable")
  val oracleDbhistoryTable = ConfigFactory.load().getString("parameter.oracle.historyTable")
  val oracleDbupdateTable = ConfigFactory.load().getString("parameter.oracle.updateTable")
  val oracleDbUrl = s"jdbc:oracle:thin:@${oracleDbHost}:${oracleDbPort}/${oracleDbDatabase}"
  val oracleDriver="oracle.jdbc.driver.OracleDriver"
  val connectionProperties = new Properties()
  connectionProperties.put("user", s"${oracleDbUserName}")
  connectionProperties.put("password", s"${oracleDbPassword}")


  val inputDir = ConfigFactory.load().getString("parameter.getUDF.inputDir")
  val outputDir = ConfigFactory.load().getString("parameter.getUDF.outputDir")
  val archiveDir = ConfigFactory.load().getString("parameter.getUDF.inputDir")
  val workingDir = ConfigFactory.load().getString("parameter.getUDF.workingDir")
  val duplicateDir = ConfigFactory.load().getString("parameter.getUDF.duplicateDir")
  val errorDir = ConfigFactory.load().getString("parameter.getUDF.errorDir")
  val holdDir = ConfigFactory.load().getString("parameter.getUDF.holdDir")




  /// Custome Schema///
  val customSchema=StructType(
    StructField("REC_TYP", StringType,true)::
      StructField("RX_TYP",StringType,true)::
      StructField("RX_DSPNSD_DT",StringType,true)::
      StructField("RX_DSPNSD_TM",StringType,true)::
      StructField("RX_REPEAT_STATUS",StringType,true)::
      StructField("EXMT_STATUS",StringType,true)::
      StructField("RX_ID",StringType,true)::
      StructField("RX_ITEM_SEQ",StringType,true)::
      StructField("SUPPLIERS_DSPNSD_DRUG_CD",StringType,true)::
      StructField("DSPNSD_DRUG_IPU_CD",StringType,true)::
      StructField("DSPNSD_DRUG_DESC",StringType,true)::
      StructField("GENERIC_USE_MARKER",IntegerType,true)::
      StructField("DSPNSD_QTY",IntegerType,true)::
      StructField("DSPNSD_UNIT_OF_QTY",StringType,true)::
      StructField("DSPNSD_DRUG_PACK_SIZE",StringType,true)::
      StructField("SUPPLIERS_PSCR_DRUG_CD",StringType,true)::
      StructField("PSCR_DRUG_IPU_CD",StringType,true)::
      StructField("PSCR_DRUG_DESC",StringType,true)::
      StructField("PSCR_QTY",IntegerType,true)::
      StructField("VERBOSE_DOSAGE",StringType,true)::
      StructField("COST_OF_DSPNSD_QTY",FloatType,true)::
      StructField("NRSG_HM_IND",IntegerType,true)::
      StructField("TRANS_GUID",StringType,true):: Nil)



}


