package com.rxcorp.gdcp.duplication

import java.util.Properties

import scala.collection.JavaConverters._
import com.google.gson.GsonBuilder
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}
import org.apache.spark.sql.SparkSession

import scala.collection.mutable.Map

case class LogTransmission(sparkSession:SparkSession) {
  /*
 def producerFun():Unit ={

   val props = new Properties ()
   props.put ("bootstrap.servers", "192.168.100.13:9092")
   props.put ("acks", "1")
   props.put ("key.serializer", "org.apache.kafka.common.serialization.StringSerializer")
   props.put ("value.serializer", "org.apache.kafka.common.serialization.StringSerializer")

   val producer = new KafkaProducer[String, String] (props)

   val topic = "test"


   for (i <- 1 to 50) {
   val record = new ProducerRecord (topic, "key" + i, "value" + i)
   producer.send (record)
 }

   producer.close ()
 }
 */

  /*this is custome message */
  def coustomProducerFun(msg:String):Unit ={

    val props = new Properties ()
    props.put ("bootstrap.servers", "192.168.56.113:9092")
    props.put ("acks", "1")
    props.put ("key.serializer", "org.apache.kafka.common.serialization.StringSerializer")
    props.put ("value.serializer", "org.apache.kafka.common.serialization.StringSerializer")

    val producer = new KafkaProducer[String, String] (props)
    val topic = "test"
    val record = new ProducerRecord (topic, "key"+ 1, msg)
    producer.send (record)
    producer.flush()
    //   producer.close ()
  }

  /* Hash map function example */
  def hashMapFunc( hm:Map[String,String]):Unit= {
    val gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation.create()
    val jsonString = gson.toJson(hm.asJava)
    coustomProducerFun(jsonString)
  }

}



